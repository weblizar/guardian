<?php        
/*	*Theme Name	: Guardian
	*Theme Core Functions and Codes
*/

	/* Get the plugin */
	if ( ! function_exists( 'guardian_theme_is_companion_active' ) ) {
	    function guardian_theme_is_companion_active() {
	        require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
	        if ( is_plugin_active(  'weblizar-companion/weblizar-companion.php' ) ) {
	            return true;
	        } else {
	            return false;
	        }
	    }
	}

	require( get_template_directory() . '/class-tgm-plugin-activation.php' );
	require( get_template_directory() . '/core/menu/default_menu_walker.php' ); // for Default Menus
	require( get_template_directory() . '/core/menu/guardian_nav_walker.php' ); // for Custom Menus	
	require( get_template_directory() . '/core/comment-function.php' );	
	require( get_template_directory() . '/core/custom-header.php' );	

		
	/*After Theme Setup*/
	add_action( 'after_setup_theme', 'guardian_setup' ); 	
	function guardian_setup() {	

		add_theme_support( 'title-tag' );
		
		/* Enable support for Woocommerce */
		add_theme_support( 'woocommerce' );
		add_theme_support( 'wc-product-gallery-zoom' );
	    add_theme_support( 'wc-product-gallery-lightbox' );
	    add_theme_support( 'wc-product-gallery-slider' );

		global $content_width;
		//content width
		if ( ! isset( $content_width ) ) $content_width = 630; //px
	
		// Load text domain for translation-ready
		load_theme_textdomain( 'guardian');
		add_theme_support( 'post-thumbnails' ); //supports featured image
		// This theme uses wp_nav_menu() in one location.
		register_nav_menu( 'primary', __( 'Primary Menu', 'guardian' ) );
		add_theme_support( 'customize-selective-refresh-widgets' );

		/* Gutenberg */
		add_theme_support( 'wp-block-styles' );
		add_theme_support( 'align-wide' );

		/* Add editor style. */
		add_theme_support( 'editor-styles' );
		add_theme_support( 'dark-editor-style' );
		
		// Logo
		add_theme_support( 'custom-logo', array(
			'width'       => 250,
			'height'      => 250,
			'flex-width'  => true,
			'flex-height' => true,
			'header-text' => array( 'site-title', 'site-description' ),
		));
			
		// theme support 	
		add_theme_support( 'automatic-feed-links'); 

		$args = array( 
			'default-color' => 'fff' 
		);
		add_theme_support( 'custom-background', $args );

		add_editor_style( 'custom-editor-style.css' );
		
	
		/*==================
		* Crop image for blog
		* ==================*/	
		//Blogs thumbs
		add_image_size('guardian_home_post_thumb',360,180,true);	
		add_image_size('guardian_small_thumbs',1170,520,true); //2-Column	
	}
	// Read more tag to formatting in blog page 
	function guardian_content_more($more) {  
		global $post;							
	   	return '<a href="'.esc_url(get_permalink()).'">'.__('read more...','guardian');'</a>';
	}   
	add_filter( 'the_content_more_link', 'guardian_content_more' );
	
	
	// Replaces the excerpt "more" text by a link
	function guardian_new_excerpt_more($more) {
       	global $post;
		return '';
	}
	add_filter('excerpt_more', 'guardian_new_excerpt_more');
	
	/*
	* Weblizar widget area
	*/
	add_action( 'widgets_init', 'guardian_widgets_init');
	function guardian_widgets_init() {
		//register_widget('wl_flickr_widget');
		/*sidebar*/
		register_sidebar( array(
			'name'          => __( 'Sidebar', 'guardian' ),
			'id'            => 'sidebar-primary',
			'description'   => __( 'The primary widget area', 'guardian' ),
			'before_widget' => '<div id="%1$s" class="col-md-12 sidebar_widget %2$s">',
			'after_widget'  => '</div><div class="clearfix margin_top3"></div>',
			'before_title'  => '<div class="col-md-12 sidebar_title"><h4>',
			'after_title'   => '</h4></div>'
		) );
		/** footer widget area **/
		register_sidebar( array(
			'name'          => __( 'Footer Widget Area', 'guardian' ),
			'id'            => 'footer-widget-area',
			'description'   => __( 'footer widget area', 'guardian' ),
			'before_widget' => '<div id="%1$s" class="col-md-3 one_fourth animate fadeInUp %2$s" data-anim-type="fadeInUp"><div class="qlinks">',
			'after_widget'  => '</div></div>',
			'before_title'  => '<h4 class="lmb">',
			'after_title'   => '</h4>',
		) );             
	}
	
	function guardian_fonts_url() {
	    $fonts_url       = '';
	    $font_families   = array();
	    $font_families[] = 'Open+Sans:600,700';
	    $font_families[] = 'Roboto:700';
	    $font_families[] = 'Raleway:600';

	    $query_args = array(
	        'family' => urlencode( implode( '|', $font_families ) ),
	    );

	    $fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );

	    return esc_url_raw( $fonts_url );
	}

	/*==================
	* Guardian theme css and js
	* ==================*/
	function guardian_scripts() {	

		wp_enqueue_style( 'guardian-font', esc_url(guardian_fonts_url()), array(), null );
		wp_enqueue_style( 'font-awesome', get_template_directory_uri(). '/css/font-awesome-5.11.2/css/all.css');
		wp_enqueue_style( 'responsive-leyouts', get_template_directory_uri() . '/css/responsive-leyouts.css');
		wp_enqueue_style( 'mainmenu-bootstrap', get_template_directory_uri() . '/css/bootstrap.css');		
		wp_enqueue_style( 'mainmenu-menu', get_template_directory_uri() . '/css/menu.css');
		wp_enqueue_style( 'mainmenu-sticky', get_template_directory_uri() . '/css/sticky.css');
		wp_enqueue_style( 'swiper-css', get_template_directory_uri() . '/css/swiper.css');
		wp_enqueue_style( 'theme-animtae-css', get_template_directory_uri() . '/css/theme-animtae.css');
		wp_enqueue_style( 'reset', get_template_directory_uri() . '/css/reset.css');		
		// carousel Slider
		wp_enqueue_style( 'carousel-style', get_template_directory_uri() . '/css/carousel.css');
		wp_enqueue_style( 'stylesheet', get_template_directory_uri() . '/style.css');		
		// Js

		 wp_enqueue_script( 'guardian-navigation', get_template_directory_uri() . '/js/navigation.js', array('jquery'), true, true );
		wp_enqueue_script( 'bootstrap', get_template_directory_uri() .'/js/bootstrap.js',array( 'jquery' ), true, true );	
		wp_enqueue_script( 'guardian-menu', get_template_directory_uri() .'/js/menu.js', array( 'jquery' ), true, true );	
		wp_enqueue_script( 'swiper', get_template_directory_uri() .'/js/swiper.js', array( 'jquery' ), true, true );	
		wp_enqueue_script( 'guardian-custom', get_template_directory_uri() .'/js/custom.js', array( 'jquery' ), true, true );

		$slider_choise = absint(get_theme_mod( 'slider_choise', '1' ));
		if ( ! empty ( $slider_choise  ) &&  $slider_choise  == '1' ) {
                $guardian_slider = true;
        } else {
                $guardian_slider = false;
        }

        $ajax_data = array(
                'guardian_slider' => $guardian_slider,
                'image_speed' => get_theme_mod( 'slider_image_speed', '2000' ),
        );

        wp_enqueue_script( 'guardian-ajax-front', get_template_directory_uri() . '/js/guardian-ajax-front.js', array( 'jquery' ), true, true );
        wp_localize_script( 'guardian-ajax-front', 'ajax_admin', array(
                'ajax_url'    => admin_url( 'admin-ajax.php' ),
                'admin_nonce' => wp_create_nonce( 'admin_ajax_nonce' ),
                'ajax_data'   => $ajax_data,
        ) );

		if ( is_singular() ) wp_enqueue_script( "comment-reply" ); 	
	}
	add_action('wp_enqueue_scripts', 'guardian_scripts');
	
	
	/*==================
	* Add Class Gravtar
	* ==================*/
	add_filter( 'get_avatar', 'guardian_gravatar_class' );
	function guardian_gravatar_class( $class ) {
	    $class = str_replace( "class='avatar", "class='author_detail_img", $class );
	    return $class;
	}
	
	/****--- Navigation for POSTS, Author, Category , Tag , Archive ---***/	
	function guardian_navigation() { ?>
		<div class='pagination'>
			<nav id="wblizar_nav">
				<span class="nav-previous"><?php previous_posts_link('&laquo; Older Post'); ?></span>
				<span class="nav-next"><?php next_posts_link('Newer Post &raquo;'); ?></span> 
			</nav>
		</div><?php
	}	
	
	/****--- Navigation for Single ---***/
	function guardian_navigation_posts(){ ?>	
		<nav id="wblizar_nav">
			<span class="nav-previous"><?php previous_post_link('&laquo; %link'); ?></span>
			<span class="nav-next"><?php next_post_link('%link &raquo;'); ?></span> 
		</nav><?php 
	}
	/* Breadcrumbs  */
	function guardian_breadcrumbs() {
	    $delimiter = '';
	    $home      = __('Home','guardian'); // text for the 'Home' link
	   
	    echo '<div class="pagenation">';
	    global $post;
	    $homeLink = esc_url(home_url());
	    echo '<a href="' . esc_url($homeLink). '">' . esc_html($home) . '</a> <i>/</i>' . esc_html($delimiter) . ' ';
	    if (is_category()) {
	        global $wp_query;
	        $cat_obj = $wp_query->get_queried_object();
	        $thisCat = $cat_obj->term_id;
	        $thisCat = get_category($thisCat);
	        $parentCat = get_category($thisCat->parent);
	        if ($thisCat->parent != 0)
	            echo wp_kses_post(get_category_parents($parentCat, TRUE, ' ' . $delimiter . ' '));
	        echo esc_html__('Archive by category "','guardian') . single_cat_title('', false) . '"' ;
	    } elseif (is_day()) {
	        echo '<a href="' . esc_url(get_year_link(get_the_time('Y'))) . '">' . esc_html(get_the_time('Y')) . '</a> <i>/</i> ' . esc_html($delimiter) . ' ';
	        echo '<a href="' . esc_url(get_month_link(get_the_time('Y'), get_the_time('m'))) . '">' . esc_html(get_the_time('F')) . '</a> <i>/</i> ' . esc_html($delimiter) . ' ';
	        echo  esc_html(get_the_time('d')) ;
	    } elseif (is_month()) {
	        echo '<a href="' . esc_url(get_year_link(get_the_time('Y'))) . '">' . esc_html(get_the_time('Y')) . '</a> <i>/</i>' . esc_html($delimiter) . ' ';
	        echo  esc_html(get_the_time('F')) ;
	    } elseif (is_year()) {
	        echo  esc_html(get_the_time('Y')) ;
	    } elseif (is_single() && !is_attachment()) {
	        if (get_post_type() != 'post') {
	            $post_type = get_post_type_object(get_post_type());
	            $slug = $post_type->rewrite;
	            echo '<a href="' . esc_url($homeLink) . '/' . esc_url($slug['slug']) . '/">' . esc_html($post_type->labels->singular_name) . '</a> <i>/</i> ' . esc_html($delimiter) . ' ';
	            echo  esc_html(get_the_title()) ;
	        } else {
	            $cat = get_the_category();
	            $cat = $cat[0];
	            echo esc_html(get_the_title()) ;
	        }
	    } elseif (!is_single() && !is_page() && get_post_type() != 'post') {
	        $post_type = get_post_type_object(get_post_type());
			$count_posts = wp_count_posts()->publish;
			if($count_posts != '') {
	        echo  esc_html($post_type->labels->singular_name) ;
			}
	    } elseif (is_attachment()) {
	        $parent = get_post($post->post_parent);
	        $cat = get_the_category($parent->ID);
	        $cat = $cat[0];
	        echo wp_kses_post(get_category_parents($cat, TRUE, ' ' . $delimiter . ' '));
	        echo '<a href="' . esc_url(get_permalink($parent)) . '">' . esc_html($parent->post_title) . '</a> <i>/</i> ' . esc_html($delimiter) . ' ';
	        echo esc_html(get_the_title()) ;
	    } elseif (is_page() && !$post->post_parent) {
	        echo  esc_html(get_the_title()) ;
	    } elseif (is_page() && $post->post_parent) {
	        $parent_id = $post->post_parent;
	        $breadcrumbs = array();
	        while ($parent_id) {
	            $page = get_page($parent_id);
	            $breadcrumbs[] = '<a href="' . esc_url(get_permalink($page->ID)) . '">' . esc_html(get_the_title($page->ID)) . '</a> <i>/</i>';
	            $parent_id = $page->post_parent;
	        }
	        $breadcrumbs = array_reverse($breadcrumbs);
	        foreach ($breadcrumbs as $crumb)
	            echo wp_kses_post($crumb) . ' ' . esc_html($delimiter) . ' ';
	        echo  esc_html(get_the_title()) ;
	    } elseif (is_search()) {
	        echo  esc_html__('Search results for "','guardian') . get_search_query() . '"' ;
	    } elseif (is_tag()) {
	        echo  esc_html__('Posts tagged "','guardian') . single_tag_title('', false) . '"';
	    } elseif (is_author()) {
	        global $author;
	        $userdata = get_userdata($author);
	        echo esc_html__('Articles posted by ','guardian') . esc_html($userdata->display_name) ;
	    } elseif (is_404()) {
	        echo esc_html__('Error 404','guardian') ;
	    }
	    if (get_query_var('paged')) {
	        if (is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author())
	            echo ' (';
	        if (is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author())
	            echo ')';
	    }
	    echo '</div>';
	}
	
	//Plugin Recommend
add_action('tgmpa_register','Guardian_plugin_recommend');
function Guardian_plugin_recommend(){
	$plugins = array(
		array(
            'name' => __('Weblizar Companion','guardian'),
            'slug' => 'weblizar-companion',
            'required' => false,
        ),
		
	);
    tgmpa( $plugins );
}

add_action( 'wp_enqueue_scripts', 'guardian_custom_css' );
function guardian_custom_css() {
	
    $output = '';

    $output     .='
    .logo img{
        height:'.esc_attr(get_theme_mod('logo_height','55')).'px;
        width:'.esc_attr(get_theme_mod('logo_width','150')).'px;
    }';

	

	//custom css
	$custom_css = get_theme_mod('custom_css');
	if ( !empty ( $custom_css ) ) { 
		$output .= get_theme_mod('custom_css') . "\n";
	}

    wp_enqueue_style( 'custom-header-style1', get_template_directory_uri() . '/css/custom-header-style.css' );
    wp_add_inline_style( 'custom-header-style1', $output );
}