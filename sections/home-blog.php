<div class="container-fluid feature_section5">	
	<div class="container">
		<?php 
	    $blog_title1 = get_theme_mod('blog_title', __('Home Blog Title','guardian'));
	    if ( ! empty ( $blog_title1 ) ) { ?>
			<h2 class="text-center">
				<?php echo esc_html( get_theme_mod( 'blog_title', __('Latest News','guardian' )) ); 
				?> 
			</h2>
		<?php 
		}
		
		$i=1;
		$args = array( 'post_type' => 'post','posts_per_page'=>3, 'post__not_in' => get_option( 'sticky_posts' ));		
		$post_type_data = new WP_Query( $args );
		if ($post_type_data-> have_posts()) {
			while($post_type_data->have_posts()):

				$post_type_data->the_post(); 	 
				?>		
				<div class="col-md-4 col-sm-6 one_third animate" data-anim-type="fadeInUp" <?php if($i==3) { echo "id='nth_child_service'"; } ?>>    
					<h4 ><a title="<?php the_title_attribute(); ?>" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>        
					<?php if(has_post_thumbnail()): 						
						$class=array('class'=>'enigma_img_responsive'); 
						the_post_thumbnail('guardian_home_post_thumb', $class); 
					endif; ?>      
					<?php the_excerpt(); ?><br /> 

					<?php 
					$read_more = get_theme_mod('read_more', __('Read More','guardian'));
                    if (!empty ($read_more)) { ?>
						<a href="<?php the_permalink(); ?>" class="lfour"><i class="fa fa-chevron-circle-right"></i> <?php echo esc_html(get_theme_mod('read_more', __('Read More','guardian')) ); ?></a>  
					<?php  } ?>
				         
				</div>
			<?php  
			$i++; 
			endwhile;  
		}  ?>
	</div>
</div><!-- end blog section5 -->
<div class="clearfix"></div>