<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><!--<![endif]-->
<html <?php language_attributes(); ?>>
<head>	
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">  
	<meta charset="<?php bloginfo('charset'); ?>" />
	
	<?php wp_head(); ?> 
</head>
<body <?php body_class(); ?>>
<?php   
if ( function_exists( 'wp_body_open' ) )
wp_body_open();
?>
<div>
<header id="header">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'guardian' ); ?></a>
	<!-- Top header bar -->
	<div id="topHeader">
		<div class="wrapper">  
			<div class="container-fluid top_nav"  >
				<div class="container">					
					<div class="col-md-12 right">
						<ul> 
							<?php 
							$contact_email = get_theme_mod('email_id');
                            $contact_phone_no = get_theme_mod('phone_no');
							if($contact_email !=''){ ?>           	
								<li class="g_email guardian_contact_email"><a href="mailto:<?php echo esc_url($contact_email); ?>"><i class="fa fa-envelope"></i> <?php echo esc_html($contact_email); ?></a></li> 
							<?php } ?>
							<?php if($contact_phone_no !=''){ ?> 
								<li class="g_contact guardian_contact_phone"><a href="tel:<?php echo esc_url($contact_phone_no); ?>"><i class="fa fa-phone"></i> + <?php echo esc_html($contact_phone_no); ?></a></li>
							<?php } ?>
							<?php 
							$header_section_social_media_enbled = absint(get_theme_mod('header_social_media_in_enabled', 1));
							if($header_section_social_media_enbled  == 1){ 
								$fb_link = get_theme_mod('fb_link');
								if (!empty ($fb_link)) { ?>
									<li class="guardian_facebook_link"><a href="<?php echo esc_url(get_theme_mod('fb_link')); ?>"><i class="fab fa-facebook-f"></i></a></li>
								<?php }  
								$twitter_link = get_theme_mod('twitter_link');
                                if (!empty ($twitter_link)) { ?>
									<li><a href="<?php echo esc_url(get_theme_mod('twitter_link')); ?>"><i class="fab fa-twitter"></i></a></li>
								
								<?php }  
                                $linkedin_link = get_theme_mod('linkedin_link');
                                if (!empty ($linkedin_link)) { ?>
									<li><a href="<?php echo esc_url(get_theme_mod('linkedin_link')); ?>"><i class="fab fa-linkedin"></i></a></li>
								<?php }  
								$vk_link = get_theme_mod('vk_link');
                                if (!empty ($vk_link)) { ?>
									<li><a href="<?php echo esc_url(get_theme_mod('vk_link')); ?>"><i class="fa fa-rss"></i></a></li>
								<?php }  
								$youtube_link = get_theme_mod('youtube_link');
                                if (!empty ($youtube_link)) { ?>
									<li><a href="<?php echo esc_url(get_theme_mod('youtube_link')); ?>"><i class="fab fa-youtube"></i></a></li>
								<?php }  
								$instagram = get_theme_mod('instagram');
                                if (!empty ($instagram)) { ?>
									<li><a href="<?php echo esc_url(get_theme_mod('instagram')); ?>"><i class="fab fa-instagram"></i></a></li>
								<?php } 
							} ?>
						</ul>					
					</div>
					<!-- end right social links -->			
				</div>
			</div>            
		</div>    
	</div><!-- end top navigation -->
	<div id="trueHeader">    
		<div class="wrapper">    
			<div class="container header-nav-bar">    
				<!-- Logo -->
				<div class="col-md-3 col-sm-3 logo">
						<?php 
						if(has_custom_logo()) { 
							the_custom_logo(); 
						} 
						if (display_header_text()==true){ ?>
							
							<a href="<?php echo esc_url(home_url( '/' )); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home" id="logo" >
							<span class="site-title"><?php echo esc_html(get_bloginfo('name')); ?></span></a>
						<?php }  
						?>						
					
					<?php if (display_header_text()==true){ ?>
						<p class="site-description"><?php bloginfo( 'description' ); ?></p>
					<?php } ?>
				</div>
				<!-- Menu -->
				<div class="col-md-9 col-sm-9 menu_main">
					<div id="site-navigation" class="main-navigation navbar yamm navbar" role="navigation">
						<div  class="navbar-header">
								<div class="navbar-toggle .navbar-collapse .pull-right " data-toggle="collapse" data-target="#navbar-collapse-1"  >
									<button type="button" ><i class="fa fa-bars"></i></button>
								</div>
							</div>
							<!-- /Navigation  menus -->
						<div id="navbar-collapse-1" class="navbar-collapse collapse">  
						<?php
						wp_nav_menu( array(  
							'theme_location'=> 'primary',
							'menu_class' 	=> 'nav navbar-nav navbar-right',
							'fallback_cb' 	=> 'guardian_fallback_page_menu',
							'walker' 		=> new guardian_nav_walker()
							)
						);	
						?>	
						</div>		
					</div>			 
				</div>
				<!-- end menu -->				
			</div>			
		</div>    
	</div>    
</header>
<div class="clearfix"></div>
<div id="content" class="site-content">