<?php 
// code for comment
if ( ! function_exists( 'guardian_comment' ) ) :
	function guardian_comment( $comment, $args, $depth ) {
		//get theme data
		global $comment_data;

		//translations
		$leave_reply = $comment_data['translation_reply_to_coment'] ? $comment_data['translation_reply_to_coment'] : 
		__('Reply','guardian'); ?>        
	    <div class="col-xs-12 comment_wrap">
			<div class="col-xs-1 gravatar">
	        	<?php echo wp_kses_post(get_avatar($comment_data,$size = '60')); ?>
	        </div>
	       <div class="col-xs-11 comment_content">
				<div class="comment_meta">		   
					<div class="comment_author"><?php esc_html(comment_author());?></div>				
				</div>
				<div class="comment_text">
					<?php esc_html(comment_text()) ; ?>
					<a href=""><?php esc_url(comment_reply_link(array_merge( $args, array('reply_text' => $leave_reply,'depth' => $depth, 'max_depth' => $args['max_depth']))))?>
					</a>
				</div>
			</div>
			<?php if ( $comment_data->comment_approved == '0' ) : ?>			
			<div id="div2" class="info">
				<div class="message-box-wrap">
					<button class="close-but" id="colosebut2"><?php esc_html_e( 'close', 'guardian' ); ?></button>
					<?php esc_html_e( 'Your comment is awaiting moderation.', 'guardian' ); ?>
				</div>
			</div>
			<br/>
			<?php endif; ?>	
		</div>		
	<?php
	}
endif;