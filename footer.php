<?php if ( is_active_sidebar( 'footer-widget-area' ) ) {  ?>
	<div class="clearfix"></div>
	<div class="container-fluid footer1">
		<div class="container">			
			<div class="clearfix divider_dashed1"></div>
			<?php dynamic_sidebar( 'footer-widget-area' ); ?>		
		</div>
	</div><!-- end footer -->
<?php } ?>

<div class="clearfix"></div>
<div class="copyright_info">
	<div class="container">
		<div class="clearfix divider_dashed10"></div>
		<div class="col-md-8 col-sm-5 one_third animate" data-anim-type="fadeInRight">			
			<?php 
			 echo esc_html( get_theme_mod( 'guardian_footer_customization', __('&copy; Copyright 2020. All Rights Reserved','guardian') )); ?>
			 <a target="_blank" rel="nofollow" href="<?php echo esc_url( get_theme_mod( 'guardian_deve_link' ) ); ?>">
               <?php echo esc_html( get_theme_mod( 'guardian_develop_by' )); ?>
            </a>					
		</div>	
		<?php 
		if ( guardian_theme_is_companion_active() ) {
		$footer_section_social_media_enbled = absint(get_theme_mod('footer_section_social_media_enbled', 1));
        if ($footer_section_social_media_enbled == 1) { ?>
			<div class="col-md-4 col-sm-4 one_third last">			
				<ul class="footer_social_links guardian_footer_section_social_media">
				<?php 
				$fb_link = get_theme_mod('fb_link');
                if (!empty ($fb_link)) { ?>
					<li class="animate" data-anim-type="zoomIn"><a href="<?php echo esc_url(get_theme_mod('fb_link')); ?>"><i class="fab fa-facebook-f"></i></a></li>
				<?php  }  
				$twitter_link = get_theme_mod('twitter_link');
                if (!empty ($twitter_link)) { ?>
					<li class="animate" data-anim-type="zoomIn"><a href="<?php echo esc_url(get_theme_mod('twitter_link')); ?>"><i class="fab fa-twitter"></i></a></li>
				
				<?php  }  
				$linkedin_link = get_theme_mod('linkedin_link');
                if (!empty ($linkedin_link)) { ?>
					<li class="animate" data-anim-type="zoomIn"><a href="<?php echo esc_url(get_theme_mod('linkedin_link')); ?>"><i class="fab fa-linkedin"></i></a></li>
				<?php  }  
				$vk_link = get_theme_mod('vk_link');
                if (!empty ($vk_link)) { ?>				
					<li class="animate" data-anim-type="zoomIn"><a href="<?php echo esc_url(get_theme_mod('vk_link')); ?>"><i class="fa fa-rss"></i></a></li>
				<?php  }  
				$youtube_link = get_theme_mod('youtube_link');
                if (!empty ($youtube_link)) { ?>		
					<li class="animate" data-anim-type="zoomIn"><a href="<?php echo esc_url(get_theme_mod('youtube_link')); ?>"><i class="fab fa-youtube"></i></a></li>
				<?php  }  
				$instagram = get_theme_mod('instagram');
                if (!empty ($instagram)) { ?>
					<li class="animate" data-anim-type="zoomIn"><a href="<?php echo esc_url(get_theme_mod('instagram')); ?>"><i class="fab fa-instagram"></i></a></li>
				<?php  }  ?>
				</ul>
			</div>	
		<?php } } ?>			
	</div>
</div><!-- end copyright info -->
	
</div></div> <!-- end of header wrapper div -->
<?php wp_footer(); ?>
<?php if ( guardian_theme_is_companion_active() ) { 
	$guardian_return_top = absint(get_theme_mod( 'guardian_return_top', '1' ));
	if ( $guardian_return_top == "1" ) { ?>
		<a href="#" class="back-to-top"><i class="fa fa-angle-up"></i></a><!-- end scroll to top of the page-->	
	<?php } 
} ?>
</body>
</html>