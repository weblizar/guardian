var window, document, jQuery;
(function (jQuery) {
    "use strict"; 

    var img_speed = ajax_admin.ajax_data.image_speed;
    
    if ( ajax_admin.ajax_data.guardian_slider == true ) {
        if( img_speed.length != 0 && img_speed != undefined ) {
            jQuery(document ).ready(function( $ ) {
                jQuery("#myCarousel" ).carousel({
                    interval:ajax_admin.ajax_data.image_speed

                });
            });
        }
    } else { 
        var swiper = new Swiper('.guardian_slider', {
            spaceBetween: 30,
            loop:true,
            autoplay: {
                delay: ajax_admin.ajax_data.image_speed,
                disableOnInteraction: false,
              },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
        });

   } 
})(jQuery);